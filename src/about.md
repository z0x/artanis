((title . "About GNU Artanis")
 (layouts "about.sxml")
 (authors "Mu Lei known as NalaGinrut <nalaginrut@gmail.com>")
 (date . 1374652887))

About GNU Artanis
=============

* What is it?

GNU Artanis is a framework for web authoring - for instance, generating HTML pages dynamically. In other words, a WAF (Web Application Framework).

* Who wrote it?

Mu Lei AKA NalaGinrut, a Certified Scheme Nut - someone who would rather face death than not use the Scheme programming language to build a website.

* Why write it?

More seriously, Artanis is written using [GNU Guile](http://www.gnu.org/software/guile/), one of the best implementations of Scheme language.

One day, the folks at GNU were discussing what language they would write the GNU website in - and many chose Python. But I found that strange, because the official extension language of GNU is GNU Guile. And I wondered aloud - why not start a brand new project to provide a web framework written with GNU Guile? To which RMS said, "It's cool, I like this idea."

But at that time, it was just an idea without a plan.

Fortunately, a few months later, the Guile community held a [hack-potluck](http://lists.gnu.org/archive/html/guile-user/2013-01/msg00007.html) to celebrate Guile2 turning two - which is a contest to write a cool program in a few weeks. And so, Artanis was born.

* History

February 2013 - Artanis born at the GNU Guile hack-potluck.

2013 - Artanis submitted to "Lisp In Summer Projects" contest. Received "Certificated awesome project award" in 2014.

August 2014 - Artanis became the official project of the [SZDIY community](http://szdiy.org) for building web services on their server.

1st January, 2015 - the first stable version Artanis-0.0.1 was [released](http://nalaginrut.com/archives/2015/01/01/%5Bann%5Dartanis-0-0-1-released%21).

19th January, 2015 - SZDIY community offers Artanis to FSF/GNU, and RMS inducts it as an official GNU project. Artanis becomes GNU Artanis.

* Who uses it?

GNU Artanis is used to build web services of the SZDIY community. Additionally, some open hardware projects in SZDIY are developing their own web services using GNU Artanis.
